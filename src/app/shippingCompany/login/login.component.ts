import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShippingCompanyService } from '../shared/shipping-company.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  login = {
    cnpj: '',
    password: '',
  };

  constructor(
    private shippingCompanyService: ShippingCompanyService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  async onSubmit() {
    try {
      const result = await this.shippingCompanyService.login(this.login);
      console.log(result);

      this.router.navigate(['']);
    } catch (err) {
      console.log(err);
    }
  }
}
