import { Component, OnInit } from '@angular/core';
import { ShippingCompanyService } from '../shared/shipping-company.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  register = {
    cnpj: '',
    name: '',
    corporateName: '',
    address: '',
    stateRegistration: '',
    contact: '',
    password: '',
  };

  constructor(private shippingCompanyService: ShippingCompanyService) {}

  ngOnInit(): void {}

  async onSubmit() {
    try {
      const result = await this.shippingCompanyService.register(this.register);
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  }
}
