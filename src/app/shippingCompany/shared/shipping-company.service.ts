import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ShippingCompanyService {
  constructor(private http: HttpClient) {}

  async login(data: any) {
    const result = await this.http
      .post<any>(`${environment.api}/auth`, data)
      .toPromise();
    if (result && result.token) {
      window.localStorage.setItem('token', result.token);
      return true;
    }
    return true;
  }

  async register(data: any) {
    const result = await this.http
      .post<any>(`${environment.api}/register`, data)
      .toPromise();
    return result;
  }
}
